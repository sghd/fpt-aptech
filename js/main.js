/**
 * Created by DatHT.
 */
// When the DOM is ready, run this function
$(document).ready(function() {
    $('a:not(.tt)').smoothScroll({offset: -70, afterScroll: function() {
        $('a:not(.tt)').removeClass('active');
        $(this).addClass('active');
        $('#main-nav').removeClass('in');
    }});

});